# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

## Data augmentation

Substitute word by contextual word embeddings - BERT

## Result

CV : 0.461

LB : 0.603

## Reference
#### [Github](https://github.com/makcedward/nlpaug)

#### [Train - Roberta](https://www.kaggle.com/hannes82/commonlit-readability-roberta-simple-baseline)

#### [Inference](https://www.kaggle.com/hannes82/commonlit-readability-roberta-inference/)