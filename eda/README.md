# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

## Data augmentation

CommonLit Redability Prize - EDA

## Reference
#### [Kaggle](https://www.kaggle.com/ruchi798/commonlit-readability-prize-eda-baseline)

#### [Kaggle](https://www.kaggle.com/ammarnassanalhajali/commonlit-readability-eda)