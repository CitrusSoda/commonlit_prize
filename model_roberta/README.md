# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

## Model

Roberta for redability

## Reference
#### [Kaggle](https://www.kaggle.com/hannes82/commonlit-readability-roberta-simple-baseline)