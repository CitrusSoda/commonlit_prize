# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

## Data augmentation

Back translation augmentation

## Result

CV : 0.483

LB : 0.573

## Reference
#### [Github](https://github.com/hhhwwwuuu/BackTranslation)

#### [Train - Roberta](https://www.kaggle.com/hannes82/commonlit-readability-roberta-simple-baseline)

#### [Inference](https://www.kaggle.com/hannes82/commonlit-readability-roberta-inference/)