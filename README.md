# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

Kaggle CommonLit Readability Prize

Link : [CommonLit Readability](https://www.kaggle.com/c/commonlitreadabilityprize/overview)

### 1. Optuna + scikit learn
[Ridge vs Lasso vs Linear Regression](./scikitlearn_optuna)

### 2. Data Augmentation
1. [Word2Vec](./word2vec_aug)
2. [Substitute word by contextual word embeddings - BERT](./bert_aug)
3. [Back Translation](./back_trans_aug)

### 3. EDA
[EDA](./eda)

### 4. Model
1. [Roberta](./model_roberta)