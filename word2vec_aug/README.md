# Kaggle CommonLit Readability

![Kaggle](https://img.shields.io/badge/kaggle-compete-blue.svg)

## Data augmentation

Data augmentation used Word2Vec

## Result

CV : 0.359

LB : 0.613

## Reference
#### [Github](https://github.com/makcedward/nlpaug)

#### [Train - Roberta](https://www.kaggle.com/hannes82/commonlit-readability-roberta-simple-baseline)

#### [Inference](https://www.kaggle.com/hannes82/commonlit-readability-roberta-inference/)